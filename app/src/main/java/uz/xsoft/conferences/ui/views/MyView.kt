package uz.xsoft.conferences.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class MyView(
    context: Context,
    attributes: AttributeSet?,
    defStyleAttr: Int
) : View(context, attributes, defStyleAttr) {
    private val mPaintGreen: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply { color = Color.GREEN }
    private val mPaintBlue: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply { color = Color.BLUE }
    private val mPaintRed: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply { color = Color.RED }
    private val mPaintYellow: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply { color = Color.YELLOW }
    private val mRectF = RectF()

    constructor(context: Context, attributes: AttributeSet?) : this(context, attributes, 0)

    constructor(context: Context) : this(context, null, 0)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        mRectF.top = 0f
        mRectF.left = 0f
        mRectF.bottom = height.toFloat() - 10f
        mRectF.right = width.toFloat() - 10f

        canvas?.drawArc(mRectF, 0f, 90f, true, mPaintGreen)
        canvas?.drawArc(mRectF, 90f, 90f, true, mPaintBlue)
        canvas?.drawArc(mRectF, 180f, 90f, true, mPaintRed)
        canvas?.drawArc(mRectF, 270f, 90f, true, mPaintYellow)

    }
}